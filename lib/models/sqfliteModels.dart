class Recipe {
  final int id;
  final String name;
  final int count;
  final int itemId; 
  
  const Recipe({
    required this.id,
    required this.name,
    required this.count,
    required this.itemId
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'count': count,
      'itemId' : itemId
    };
  }
  @override
  String toString() {
    return 'Dog{id: $id, name: $name, count: $count, itemId: $itemId}';
  }
}