import 'dart:async';
import 'package:acumen/models/sqfliteModels.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DBHelper{
  static var _db;

  Future<Database> get db async {
    if(_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  initDb() async {
    var theDb;
    try {
      var databasesPath = await getDatabasesPath();
      String path = join(databasesPath, "acumen_database.db");
      theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    } catch (e) {
      debugPrint(e.toString());
    }
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
      'CREATE TABLE recipe(id INTEGER PRIMARY KEY, name TEXT, count INTEGER)',
    );
    print("Created tables");
  }

  Future<List<Recipe>> getRecipes() async {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM Recipes');
    List<Recipe> recipes = [];
    for (int i = 0; i < list.length; i++) {
      recipes.add(Recipe(
          count: list[i]["count"],
          id: list[i]["id"],
          name: list[i]["name"],
          itemId: list[i]["itemId"]
      ));
    }
    return recipes;
  }

//  void saveEmployee(Employee employee) async {
 //   var dbClient = await db;
  //  await dbClient.transaction((txn) async {
 //     return await txn.rawInsert(
  //        'INSERT INTO Employee(firstname, lastname, mobileno, emailid ) VALUES(' +
 //             '\'' +
 //             employee.firstName +
 //             '\'' +
 //             ',' +
 //             '\'' +
 //             employee.lastName +
  //            '\'' +
   //           ',' +
  //            '\'' +
   //           employee.mobileNo +
    //          '\'' +
   //           ',' +
   //           '\'' +
   //           employee.emailId +
   //           '\'' +
   //           ')');
  //  });
 // }
}